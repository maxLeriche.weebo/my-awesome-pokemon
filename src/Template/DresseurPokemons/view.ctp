<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DresseurPokemon $dresseurPokemon
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Dresseur Pokemon'), ['action' => 'edit', $dresseurPokemon->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Dresseur Pokemon'), ['action' => 'delete', $dresseurPokemon->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dresseurPokemon->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Dresseur Pokemons'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dresseur Pokemon'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dresseurs'), ['controller' => 'Dresseurs', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dresseur'), ['controller' => 'Dresseurs', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pokes'), ['controller' => 'Pokes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Poke'), ['controller' => 'Pokes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="dresseurPokemons view large-9 medium-8 columns content">
    <h3><?= h($dresseurPokemon->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Dresseur') ?></th>
            <td><?= $dresseurPokemon->has('dresseur') ? $this->Html->link($dresseurPokemon->dresseur->id, ['controller' => 'Dresseurs', 'action' => 'view', $dresseurPokemon->dresseur->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Poke') ?></th>
            <td><?= $dresseurPokemon->has('poke') ? $this->Html->link($dresseurPokemon->poke->name, ['controller' => 'Pokes', 'action' => 'view', $dresseurPokemon->poke->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($dresseurPokemon->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($dresseurPokemon->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($dresseurPokemon->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Favoris') ?></th>
            <td><?= $dresseurPokemon->favoris ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
</div>
