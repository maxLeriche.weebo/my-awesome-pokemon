<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DresseurPokemon[]|\Cake\Collection\CollectionInterface $dresseurPokemons
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Dresseur Pokemon'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dresseurs'), ['controller' => 'Dresseurs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dresseur'), ['controller' => 'Dresseurs', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pokes'), ['controller' => 'Pokes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Poke'), ['controller' => 'Pokes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="dresseurPokemons index large-9 medium-8 columns content">
    <h3><?= __('Dresseur Pokemons') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('dresseur_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('poke_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('favoris') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($dresseurPokemons as $dresseurPokemon): ?>
            <tr>
                <td><?= $this->Number->format($dresseurPokemon->id) ?></td>
                <td><?= $dresseurPokemon->has('dresseur') ? $this->Html->link($dresseurPokemon->dresseur->id, ['controller' => 'Dresseurs', 'action' => 'view', $dresseurPokemon->dresseur->id]) : '' ?></td>
                <td><?= $dresseurPokemon->has('poke') ? $this->Html->link($dresseurPokemon->poke->name, ['controller' => 'Pokes', 'action' => 'view', $dresseurPokemon->poke->id]) : '' ?></td>
                <td><?= h($dresseurPokemon->favoris) ?></td>
                <td><?= h($dresseurPokemon->created) ?></td>
                <td><?= h($dresseurPokemon->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $dresseurPokemon->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $dresseurPokemon->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $dresseurPokemon->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dresseurPokemon->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
