<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Dresseur $dresseur
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Dresseur'), ['action' => 'edit', $dresseur->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Dresseur'), ['action' => 'delete', $dresseur->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dresseur->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Dresseurs'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dresseur'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dresseur Pokemons'), ['controller' => 'DresseurPokemons', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dresseur Pokemon'), ['controller' => 'DresseurPokemons', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="dresseurs view large-9 medium-8 columns content">
    <h3><?= h($dresseur->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($dresseur->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($dresseur->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($dresseur->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Nom') ?></h4>
        <?= $this->Text->autoParagraph(h($dresseur->nom)); ?>
    </div>
    <div class="row">
        <h4><?= __('Prenom') ?></h4>
        <?= $this->Text->autoParagraph(h($dresseur->prenom)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Dresseur Pokemons') ?></h4>
        <?php if (!empty($dresseur->dresseur_pokemons)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Dresseur Id') ?></th>
                <th scope="col"><?= __('Poke Id') ?></th>
                <th scope="col"><?= __('Favoris') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($dresseur->dresseur_pokemons as $dresseurPokemons): ?>
            <tr>
                <td><?= h($dresseurPokemons->id) ?></td>
                <td><?= h($dresseurPokemons->dresseur_id) ?></td>
                <td><?= h($dresseurPokemons->poke_id) ?></td>
                <td><?= h($dresseurPokemons->favoris) ?></td>
                <td><?= h($dresseurPokemons->created) ?></td>
                <td><?= h($dresseurPokemons->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'DresseurPokemons', 'action' => 'view', $dresseurPokemons->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'DresseurPokemons', 'action' => 'edit', $dresseurPokemons->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'DresseurPokemons', 'action' => 'delete', $dresseurPokemons->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dresseurPokemons->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
