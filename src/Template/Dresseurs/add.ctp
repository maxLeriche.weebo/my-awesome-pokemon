<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Dresseur $dresseur
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Dresseurs'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Pokes'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Dresseur Pokemons'), ['controller' => 'DresseurPokemons', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dresseur Pokemon'), ['controller' => 'DresseurPokemons', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="dresseurs form large-9 medium-8 columns content">
    <?= $this->Form->create($dresseur) ?>
    <fieldset>
        <legend><?= __('Add Dresseur') ?></legend>
        <?php
            echo $this->Form->control('nom');
            echo $this->Form->control('prenom');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
