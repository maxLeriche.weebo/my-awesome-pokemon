<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Dresseurs Controller
 *
 * @property \App\Model\Table\DresseursTable $Dresseurs
 *
 * @method \App\Model\Entity\Dresseur[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DresseursController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $dresseurs = $this->paginate($this->Dresseurs);

        $this->set(compact('dresseurs'));
    }

    /**
     * View method
     *
     * @param string|null $id Dresseur id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $dresseur = $this->Dresseurs->get($id, [
            'contain' => ['DresseurPokemons']
        ]);

        $this->set('dresseur', $dresseur);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $dresseur = $this->Dresseurs->newEntity();
        if ($this->request->is('post')) {
            $dresseur = $this->Dresseurs->patchEntity($dresseur, $this->request->getData());
            if ($this->Dresseurs->save($dresseur)) {
                $this->Flash->success(__('The dresseur has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The dresseur could not be saved. Please, try again.'));
        }
        $this->set(compact('dresseur'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Dresseur id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $dresseur = $this->Dresseurs->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $dresseur = $this->Dresseurs->patchEntity($dresseur, $this->request->getData());
            if ($this->Dresseurs->save($dresseur)) {
                $this->Flash->success(__('The dresseur has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The dresseur could not be saved. Please, try again.'));
        }
        $this->set(compact('dresseur'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Dresseur id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $dresseur = $this->Dresseurs->get($id);
        if ($this->Dresseurs->delete($dresseur)) {
            $this->Flash->success(__('The dresseur has been deleted.'));
        } else {
            $this->Flash->error(__('The dresseur could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function fight($drone=NULL,$drtwo=NULL)
    {
        $prone=false;
        $prtwo=false;
        $winner="NO ONE";
        if(isset($drone)&&$drone!=NULL)
        {
            $dresseur1 = $this->Dresseurs->get($drone, [
                'contain' => ['DresseurPokemons']
            ]);
            $prone=true;
        }
        if(isset($drtwo)&&$drtwo!=NULL)
        {
            $dresseur2 = $this->Dresseurs->get($drtwo, [
                'contain' => ['DresseurPokemons']
            ]);
            $prtwo=true;
        }
        if($prone&&$prtwo)
        {
            $boucle=true;
            $pvDRone=100;
            $pvDRtwo=100;
            $whowin="noone";
            while($boucle)
            {
                $DGTone=rand(0,20);
                $pvDRtwo=$pvDRtwo-$DGTone;
                if($pvDRtwo>0)
                {
                    $DGTtwo=rand(0,20);
                    $pvDRone=$pvDRone-$DGTtwo;
                    if($pvDRone<=0)
                    {
                        $boucle=false;
                        $whowin=$dresseur2;
                    }
                }
                else
                {
                    $boucle=false;
                    $whowin=$dresseur1;
                }
                
            }
            $winner=array(
                'winner' => $whowin,
                'pvdrone' =>$pvDRone,
                'pvdrtwo' =>$pvDRtwo
            );
        }
        $this ->set(compact('dresseur1','dresseur2','winner'));
    }
    
}
